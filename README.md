# fw-form-companion

This is a Firewall Form Companion code for a web server that will parse Firewall data and compare VRF ID's and generate a .docx request form based on the results of the comparison

## Getting started

If running locally CORS Extension is required to be enabled for this project to work. It can be found on Chrome or Firefox by searching "CORS Extension".

To view the webpage run fw_app_ui using "go run .", found at http://127.0.0.1:3000/static/frontend.html

If running locally you have to run fw_app using "go run ." in order to build the CORS connection

You can input any desired IP information into data_ing
When data_ing is updated with new values refresh the accessible list at http://127.0.0.1:8000/dataing/True

For testing purposes example IP's have been already input.
- same VRF: 192.33.63.0/24 and 192.33.61.0/24 
- different VRF: 192.33.61.0/24 and 192.33.44.0/24 
- error: any non-recognized VRF

Generate FW Request will build a formatted .docx based on the data under Generated Tags
Leaving all fields empty will generate an empty Firewall Request Form
