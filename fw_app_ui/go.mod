module fw_app_ui.echodata.tv/mod

go 1.18

require github.com/gorilla/mux v1.8.0

require github.com/gorilla/handlers v1.5.1

require github.com/felixge/httpsnoop v1.0.3 // indirect
