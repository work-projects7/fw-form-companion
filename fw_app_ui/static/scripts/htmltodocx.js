/*
    This file is for converting the data retreived in main.js into a .docx format.
    It's mostly formatting tables using npm docx.js 
    You can find the documentation at:
        https://github.com/dolanmiu/docx/tree/master/docs/usage
        https://docx.js.org/#/usage/bullet-points
*/

import { elemArr } from "./main.js";

export function generate() {
    var nameTable = genNameTable();
    var projTable = genProjTable();
    var descTable = genDescTable();
    var dataTable = genDataTable();

    var child = [];

    child.push(
        new docx.Paragraph({
            text: "Firewall Access Request Form",
            style: "title",
        })
    );
    child.push(
        new docx.Paragraph({
            text: "Submit this form at http://upl-jira.uplinkdata.com/servicedesk/customer/portal/1",
            bullet: {
                level: 0
            },
            style: "header",
            spacing: {
                after: 100,
            },
        })
    );

    child.push(nameTable);
    child.push(new docx.Paragraph(""));
    child.push(projTable);
    child.push(new docx.Paragraph(""));
    child.push(descTable);
    child.push(new docx.Paragraph(""));
    child.push(dataTable);

    if (elemArr.length == 0) {
        var directionsTable = genDirectionsTable();
        child.push(new docx.Paragraph(""));
        child.push(directionsTable);
    }

    const doc = new docx.Document({
        styles: {
            paragraphStyles: [{
                id: "title",
                name: "Title",
                basedOn: "Normal",
                next: "Normal",
                run: {
                  font: {
                    name: "Arial",
                  },
                  bold: true,
                  size: 40,
                  },
              },
            {
              id: "header",
              name: "Header",
              basedOn: "Normal",
              next: "Normal",
              run: {
                font: {
                  name: "Arial",
                },
                bold: true,
                size: 18,
                },
            },
            {
                id: "body",
                name: "Body",
                basedOn: "Normal",
                next: "Normal",
                run: {
                  font: {
                    name: "Arial",
                  },
                  size: 24,
                  },
              }],
          },
        sections: [{
            properties: {
                page: {
                    size: {
                        orientation: docx.PageOrientation.LANDSCAPE,
                    },
                },
            },
            children: child,
        }]
      });
      
      docx.Packer.toBlob(doc).then(blob => {
        saveAs(blob, "Firewall-Request-Form.docx");
        console.log("Document created successfully");
      });
}

function genNameTable() {
    const reqName = document.querySelector('#name').value;
    const reqEmail = document.querySelector('#email').value;
    const reqPhoneNum = document.querySelector('#phone_num').value;
    const department = document.querySelector('#dept_name').value;
    const date = document.querySelector('#date').value;

    const table = new docx.Table({
        columnWidths: [2770,2770,4608,1946,1946],
        rows: [
            new docx.TableRow({
                children: [
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: "Requestor's Name",
                            style: "header",
                            alignment: docx.AlignmentType.CENTER,
                        })],
                        width: {
                            size: 2770,
                            type: docx.WidthType.DXA,
                        },
                        shading: {
                            fill: "c6d9f1"
                        },
                        margins: {
                            top: 10,
                            bottom: 10,
                        },
                    }),
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: "Requestor's Phone",
                            style: "header",
                            alignment: docx.AlignmentType.CENTER,
                        })],
                        width: {
                            size: 2770,
                            type: docx.WidthType.DXA,
                        },
                        shading: {
                            fill: "c6d9f1"
                        },
                        margins: {
                            top: 10,
                            bottom: 10,
                        },
                    }),
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: "Requestor's Email",
                            style: "header",
                            alignment: docx.AlignmentType.CENTER,
                        })],
                        width: {
                            size: 4608,
                            type: docx.WidthType.DXA,
                        },
                        shading: {
                            fill: "c6d9f1"
                        },
                        margins: {
                            top: 10,
                            bottom: 10,
                        },
                    }),
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: "Department",
                            style: "header",
                            alignment: docx.AlignmentType.CENTER,
                        })],
                        width: {
                            size: 1946,
                            type: docx.WidthType.DXA,
                        },
                        shading: {
                            fill: "c6d9f1"
                        },
                        margins: {
                            top: 10,
                            bottom: 10,
                        },
                    }),
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: "Request by Date",
                            style: "header",
                            alignment: docx.AlignmentType.CENTER,
                        })],
                        width: {
                            size: 1946,
                            type: docx.WidthType.DXA,
                        },
                        shading: {
                            fill: "c6d9f1"
                        },
                        margins: {
                            top: 10,
                            bottom: 10,
                        },
                    }),
                ],
            }),
            new docx.TableRow({
                children: [
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: reqName,
                            style: "body",
                        })],
                        width: {
                            size: 2770,
                            type: docx.WidthType.DXA,
                        },
                        margins: {
                            top: 20,
                            bottom: 20,
                        },
                    }),
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: reqPhoneNum,
                            style: "body",
                        })],
                        width: {
                            size: 2770,
                            type: docx.WidthType.DXA,
                        },
                        margins: {
                            top: 20,
                            bottom: 20,
                        },
                    }),
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: reqEmail,
                            style: "body",
                        })],
                        width: {
                            size: 4608,
                            type: docx.WidthType.DXA,
                        },
                        margins: {
                            top: 20,
                            bottom: 20,
                        },
                    }),
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: department,
                            style: "body",
                        })],
                        width: {
                            size: 1946,
                            type: docx.WidthType.DXA,
                        },
                        margins: {
                            top: 20,
                            bottom: 20,
                        },
                    }),
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: date,
                            style: "body",
                        })],
                        width: {
                            size: 1946,
                            type: docx.WidthType.DXA,
                        },
                        margins: {
                            top: 20,
                            bottom: 20,
                        },
                    }),
                ],
            }),
        ],
    });

    return table;
}

function genProjTable() {
    const ticketNum = document.querySelector('#ticket_name').value;
    const projName = document.querySelector('#proj_name').value;
    const manName = document.querySelector('#manager_name').value;

    const table = new docx.Table({
        columnWidths: [2770,2770,8500],
        rows: [
            new docx.TableRow({
                children: [
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: "Manager's Name",
                            style: "header",
                            alignment: docx.AlignmentType.CENTER,
                        })],
                        width: {
                            size: 2770,
                            type: docx.WidthType.DXA,
                        },
                        shading: {
                            fill: "c6d9f1"
                        },
                        margins: {
                            top: 10,
                            bottom: 10,
                        },
                    }),
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: "Ticket Number",
                            style: "header",
                            alignment: docx.AlignmentType.CENTER,
                        })],
                        width: {
                            size: 2770,
                            type: docx.WidthType.DXA,
                        },
                        shading: {
                            fill: "c6d9f1"
                        },
                        margins: {
                            top: 10,
                            bottom: 10,
                        },
                    }),
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: "Project or Vendor the request is for",
                            style: "header",
                            alignment: docx.AlignmentType.CENTER,
                        })],
                        width: {
                            size: 8500,
                            type: docx.WidthType.DXA,
                        },
                        shading: {
                            fill: "c6d9f1"
                        },
                        margins: {
                            top: 10,
                            bottom: 10,
                        },
                    }),
                ],
            }),
            new docx.TableRow({
                children: [
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: manName,
                            style: "body",
                        })],
                        width: {
                            size: 2770,
                            type: docx.WidthType.DXA,
                        },
                        margins: {
                            top: 20,
                            bottom: 20,
                        },
                    }),
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: ticketNum,
                            style: "body",
                        })],
                        width: {
                            size: 2770,
                            type: docx.WidthType.DXA,
                        },
                        margins: {
                            top: 20,
                            bottom: 20,
                        },
                    }),
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: projName,
                            style: "body",
                        })],
                        width: {
                            size: 8500,
                            type: docx.WidthType.DXA,
                        },
                        margins: {
                            top: 20,
                            bottom: 20,
                        },
                    }),
                ],
            }),
        ],
    });

    return table;
}

function genDescTable() {
    const descData = document.querySelector('#acc_desc').value;

    const table = new docx.Table({
        columnWidths: [14040],
        rows: [
            new docx.TableRow({
                children: [
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: "Description for why access is needed",
                            style: "header",
                            alignment: docx.AlignmentType.CENTER,
                        })],
                        width: {
                            size: 14040,
                            type: docx.WidthType.DXA,
                        },
                        shading: {
                            fill: "c6d9f1"
                        },
                        margins: {
                            top: 10,
                            bottom: 10,
                        },
                    }),
                ],
            }),
            new docx.TableRow({
                children: [
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: descData,
                            style: "body",
                        })],
                        width: {
                            size: 14040,
                            type: docx.WidthType.DXA,
                        },
                        margins: {
                            top: 20,
                            bottom: 20,
                        },
                    }),
                ],
            }),
        ],
    });
    return table;
}


function genDataTable() {
    var row = genRowData();

    const table = new docx.Table({
        columnWidths: [500,1230,1630,3080,1680,1680,1530,1580,1130],
        rows: row
  });
    return table;
}

function genRowData() {
    var row = [];

    row.push(
        new docx.TableRow({
            tableHeader: true,
            children: [
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "#",
                        style: "header",
                        alignment: docx.AlignmentType.CENTER,
                    })],
                    width: {
                        size: 500,
                        type: docx.WidthType.DXA,
                    },
                    shading: {
                        fill: "c6d9f1"
                    },
                    margins: {
                        top: 10,
                        bottom: 10,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "Add/Delete Rule",
                        style: "header",
                        alignment: docx.AlignmentType.CENTER,
                    })],
                    width: {
                        size: 1230,
                        type: docx.WidthType.DXA,
                    },
                    shading: {
                        fill: "c6d9f1"
                    },
                    margins: {
                        top: 10,
                        bottom: 10,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "Application",
                        style: "header",
                        alignment: docx.AlignmentType.CENTER,
                    })],
                    width: {
                        size: 1630,
                        type: docx.WidthType.DXA,
                    },
                    shading: {
                        fill: "c6d9f1"
                    },
                    margins: {
                        top: 10,
                        bottom: 10,
                    },
                }),
            new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "Description",
                        style: "header",
                        alignment: docx.AlignmentType.CENTER,
                    })],
                    width: {
                        size: 3130,
                        type: docx.WidthType.DXA,
                    },
                    shading: {
                        fill: "c6d9f1"
                    },
                    margins: {
                        top: 10,
                        bottom: 10,
                    },
                }),
            new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "Source IP Address",
                        style: "header",
                        alignment: docx.AlignmentType.CENTER,
                    })],
                    width: {
                        size: 1680,
                        type: docx.WidthType.DXA,
                    },
                    shading: {
                        fill: "c6d9f1"
                    },
                    margins: {
                        top: 10,
                        bottom: 10,
                    },
                }),
            new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "Destination IP Address",
                        style: "header",
                        alignment: docx.AlignmentType.CENTER,
                    })],
                    width: {
                        size: 1680,
                        type: docx.WidthType.DXA,
                    },
                    shading: {
                        fill: "c6d9f1"
                    },
                    margins: {
                        top: 10,
                        bottom: 10,
                    },
                }),
            new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "Protocol/Port",
                        style: "header",
                        alignment: docx.AlignmentType.CENTER,
                    })],
                    width: {
                        size: 1530,
                        type: docx.WidthType.DXA,
                    },
                    shading: {
                        fill: "c6d9f1"
                    },
                    margins: {
                        top: 10,
                        bottom: 10,
                    },
                }),
            new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "Rule Action: Accept/Deny",
                        style: "header",
                        alignment: docx.AlignmentType.CENTER,
                    })],
                    width: {
                        size: 1630,
                        type: docx.WidthType.DXA,
                    },
                    shading: {
                        fill: "c6d9f1"
                    },
                    margins: {
                        top: 10,
                        bottom: 10,
                    },
                }),
            new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "Public IP Needed",
                        style: "header",
                        alignment: docx.AlignmentType.CENTER,
                    })],
                    width: {
                        size: 1130,
                        type: docx.WidthType.DXA,
                    },
                    shading: {
                        fill: "c6d9f1"
                    },
                    margins: {
                        top: 10,
                        bottom: 10,
                    },
                }),
            ],
        })
    );

    if(elemArr.length == 0) {
        row.push(genEmptyDataTable());
    }

    for(let k in elemArr) {
        var addDelElem = elemArr[k].querySelector('#addDelete').innerHTML;
        var appElem = elemArr[k].querySelector('#appData').innerHTML;
        var descElem = elemArr[k].querySelector('#descData').innerHTML;
        var sourceElem = elemArr[k].querySelector('#sourceData').innerHTML;
        var destElem = elemArr[k].querySelector('#destData').innerHTML;
        var portElem = elemArr[k].querySelector('#portData').innerHTML;
        var ruleActElem = elemArr[k].querySelector('#ruleAction').innerHTML;
        var pubElem = elemArr[k].querySelector('#pubIP').innerHTML;
        var rowNum = parseInt(k) + 1;

        var temp = new docx.TableRow({
            children: [
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: rowNum.toString(),
                        style: "body",
                    })],
                    width: {
                        size: 500,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: addDelElem,
                        style: "body",
                    })],
                    width: {
                      size: 1230,
                      type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: appElem,
                        style: "body",
                    })],
                    width: {
                        size: 1630,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: descElem,
                        style: "body",
                    })],
                    width: {
                        size: 3130,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: sourceElem,
                        style: "body",
                    })],
                    width: {
                        size: 1680,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: destElem,
                        style: "body",
                    })],
                    width: {
                        size: 1680,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: portElem,
                        style: "body",
                    })],
                    width: {
                        size: 1530,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text:   ruleActElem,
                        style: "body",
                    })],
                    width: {
                        size: 1630,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: pubElem,
                        style: "body",
                    })],
                    width: {
                        size: 1130,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
            ],
        });
        row.push(temp);
    };

    return row;
}

function genDirectionsTable() {
    const table = new docx.Table({
        columnWidths: [14040],
        rows: [
            new docx.TableRow({
                children: [
                    new docx.TableCell({
                        children: [new docx.Paragraph({
                            text: "Dirextions",
                            style: "header",
                            alignment: docx.AlignmentType.CENTER,
                        })],
                        width: {
                            size: 14040,
                            type: docx.WidthType.DXA,
                        },
                        shading: {
                            fill: "c6d9f1"
                        },
                        margins: {
                            top: 10,
                            bottom: 10,
                        },
                    }),
                ],
            }),
            new docx.TableRow({
                children: [
                    new docx.TableCell({
                        children: [
                            new docx.Paragraph({
                                text: "Be as specific as possible when defining IPs and Protocol/Port required for connections.",
                                bullet: {
                                    level: 0
                                },
                                style: "header",
                            }),
                            new docx.Paragraph({
                                text: "Overly broad requests (e.g. use of “any” or large network or port ranges) may require extra review and possible rework of requirements to align with security best practices.",
                                bullet: {
                                    level: 1
                                },
                                style: "header",
                            }),
                            new docx.Paragraph({
                                text: "Modifications to existing firewall rules need to list Added and Deleted elements on separate lines.",
                                bullet: {
                                    level: 0
                                },
                                style: "header",
                            }),
                            new docx.Paragraph({
                                text: "Incomplete forms will not be processed.",
                                bullet: {
                                    level: 0
                                },
                                style: "header",
                            }),
                        ],
                        width: {
                            size: 14040,
                            type: docx.WidthType.DXA,
                        },
                        margins: {
                            top: 20,
                            bottom: 20,
                        },
                    }),
                ],
            }),
        ],
    });
    return table;
}

function genEmptyDataTable() {
        var temp = new docx.TableRow({
            children: [
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "",
                        style: "body",
                    })],
                    width: {
                        size: 500,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "",
                        style: "body",
                    })],
                    width: {
                      size: 1230,
                      type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "",
                        style: "body",
                    })],
                    width: {
                        size: 1630,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "",
                        style: "body",
                    })],
                    width: {
                        size: 3130,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "",
                        style: "body",
                    })],
                    width: {
                        size: 1680,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "",
                        style: "body",
                    })],
                    width: {
                        size: 1680,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "",
                        style: "body",
                    })],
                    width: {
                        size: 1530,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text:   "",
                        style: "body",
                    })],
                    width: {
                        size: 1630,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
                new docx.TableCell({
                    children: [new docx.Paragraph({
                        text: "",
                        style: "body",
                    })],
                    width: {
                        size: 1130,
                        type: docx.WidthType.DXA,
                    },
                    margins: {
                        top: 20,
                        bottom: 20,
                    },
                }),
            ],
        });
        return temp;
}