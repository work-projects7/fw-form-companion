import { generate } from "./htmltodocx.js";

// query the Add New Row button for use and output for pushing data to web server
const fetchDataBtn = document.querySelector('#fetchData');
const output = document.querySelector('#output');

// create num var for tracking rows, and elemArr for storing generated data
var num = 1;
export var elemArr = [];


async function getData(ipSource, ipDest, sIP, dIP) {
    // query html box data
    var statBox = document.querySelector('#statBox');
    var boxData = document.querySelector('#boxData');
    // query use input values
    const addDelRule = document.querySelector('#ad_rule').value;
    const appName = document.querySelector('#app_name').value;
    const desc = document.querySelector('#desc').value;
    const portNums = document.querySelector('#port_num').value;
    
    // problem reporting for the two check boxes
     try {
        document.querySelector('input[name=acc_deny]:checked').value;
    } catch (error) {
        statBox.innerHTML = statBox.innerHTML.replace(statBox.innerHTML, "Problem: ensure a 'Rule Action' option is selected, this value can be changed after .docx is produced");
        statBox.className = "static-box-error";
        return;
    }
    try {
        document.querySelector('input[name=yes_no]:checked').value;
    } catch (error) {
        statBox.innerHTML = statBox.innerHTML.replace(statBox.innerHTML, "Problem: ensure a 'Need Public IP' option is selected, this value can be changed after .docx is produced");
        statBox.className = "static-box-error";
        return;
    }
    const ruleAction = document.querySelector('input[name=acc_deny]:checked').value;
    const pubIp = document.querySelector('input[name=yes_no]:checked').value;

    

    // fetch the VRF data from webserver
    async function fetchData(ip) {
        var outputArr = [];
        const res = await fetch('http://127.0.0.1:8000/iprequest/' + ip);
        const resultJson = await res.json();
        for(let k in resultJson) {
            outputArr.push(resultJson[k]);
        }
        return outputArr;
    }

    // push output to array vars
    var destOutput = await fetchData(ipDest);
    var sourceOutput = await fetchData(ipSource);

    // "error" check to see if ip's are valid
    if(sourceOutput[1] == null) {
        var badSource = sIP + " - Not a valid IP for the request";
        statBox.innerHTML = statBox.innerHTML.replace(statBox.innerHTML, badSource);
        statBox.className = "static-box-error";
        return;
    } else if(destOutput[1] == null) {
        var badDest = dIP + " - Not a valid IP for the request";
        statBox.innerHTML = statBox.innerHTML.replace(statBox.innerHTML, badDest);
        statBox.className = "static-box-error";
        return;
    }
    
    // compare VRF for same or different
    if(destOutput[1] != sourceOutput[1]) {
        // clear previous IP's
        document.getElementById('source_ip').value = '';
        document.getElementById('dest_ip').value = '';

        // clone hidData format
        var hidData = document.querySelector('#hidData');
        var cloneHidData = hidData.cloneNode(true);
        
        // push cloneHidData to elemArr for .docx output
        elemArr.push(cloneHidData);

        // query cloneHidData elemetns
        var addDelData = cloneHidData.querySelector('#addDelete');
        var appData = cloneHidData.querySelector('#appData');
        var numData = cloneHidData.querySelector('#numData');
        var descData = cloneHidData.querySelector('#descData');
        var sourceData = cloneHidData.querySelector('#sourceData');
        var destData = cloneHidData.querySelector('#destData');
        var portData = cloneHidData.querySelector('#portData');
        var ruleActData = cloneHidData.querySelector('#ruleAction');
        var pubIpData = cloneHidData.querySelector('#pubIP');

        boxData.style.height = "auto";
    
        // push the user input to the cloneHidData field
        appData.innerHTML = appData.innerHTML.replace(appData.innerHTML, appName);
        numData.innerHTML = numData.innerHTML.replace(numData.innerHTML, num.toString());
        descData.innerHTML = descData.innerHTML.replace(descData.innerHTML, desc);
        sourceData.innerHTML = sourceData.innerHTML.replace(sourceData.innerHTML, sIP);
        destData.innerHTML = destData.innerHTML.replace(destData.innerHTML, dIP);
        portData.innerHTML = portData.innerHTML.replace(portData.innerHTML, portNums);
        addDelData.innerHTML = addDelData.innerHTML.replace(addDelData.innerHTML, addDelRule);
        pubIpData.innerHTML = pubIpData.innerHTML.replace(pubIpData.innerHTML, pubIp);
        ruleActData.innerHTML = ruleActData.innerHTML.replace(ruleActData.innerHTML, ruleAction);

        // Change the style of the boxes for every even num
        if(num % 2 == 0) {
            cloneHidData.className = "hidden-data-even";
            appData.className = "inner-box-even";
            descData.className = "inner-box-even";
            sourceData.className = "inner-box-even";
            destData.className = "inner-box-even";
            portData.className = "inner-box-even";
            addDelData.className = "inner-box-even";
            pubIpData.className = "inner-box-even";
            ruleActData.className = "inner-box-even";
        } 

        // display cloneHidData under Generated Tags field
        cloneHidData.style.display = "inline-block";
        output.before(cloneHidData);

        // valid box, two different VRFs, update color and html
        var actString = "Added Firewall Tag";
        statBox.innerHTML = statBox.innerHTML.replace(statBox.innerHTML, actString);
        statBox.className = "static-box-fail";

        // track num for display
        num = num + 1;

        // add remove functionality to listed data
        const remove = cloneHidData.querySelector('#removeData');
        remove.addEventListener("click", removeData);
        function removeData() {
           var index = parseInt(numData.innerHTML);
           elemArr.splice(index - 1, 1);
           cloneHidData.remove();            
        }
    } else {
        // valid box, same VRF, update text and color
        boxData.style.height = "auto";
        var noActString = "No FireWall Form Needed";
        statBox.innerHTML = statBox.innerHTML.replace(statBox.innerHTML, noActString);
        statBox.className = "static-box-success";
    }
};

// Add New Row event listener
fetchDataBtn.addEventListener("click", biDirectCheck);

function biDirectCheck() {
    // query user ip input's
    var ipSourceVal = document.querySelector('#source_ip').value;
    var ipDestVal = document.querySelector('#dest_ip').value;
    const biDirect = document.querySelector('#bidirect');

    // parse the IP to allow for ranges
    var ipSource = parseIpDash(ipSourceVal);
    var ipDest = parseIpDash(ipDestVal);
    ipSource = parseIpSlash(ipSourceVal);
    ipDest = parseIpSlash(ipDestVal);

    // bidirectional check
    if(biDirect.checked) {
        getData(ipSource, ipDest, ipSourceVal, ipDestVal);
        getData(ipDest, ipSource, ipDestVal, ipSourceVal);
    } else {
        getData(ipSource, ipDest, ipSourceVal, ipDestVal);
    }
}

function parseIpDash(ip) {
    // split the "-"
    var split = ip.split('-');
    return split[0];
}

function parseIpSlash(ip) {
    // split the "/"
    var splitSlash = ip.split('/');
    // split the "."
    var split = splitSlash[0].split('.');
    // increment last value
    split[split.length - 1]++;
    var out = "";
    // re-parse into string
    for(let k in split) {
        out += split[k] + '.';
    }
    //remove final .
    out = out.substring(0, out.length - 1);
    return out;
}

// Generate FW Request button event listener
const genForm = document.querySelector('#gen-form');
genForm.addEventListener("click", generate);