package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func FileParse(filePath string, VrfReturn *DataReturned) {
	VrfReturn.Data = []DataStruct{}
	files, err := ioutil.ReadDir(filePath)
	if err != nil {
		log.Println("Directory does not exist or cannot be read: no files will be ingested :", err)
	} else {
		// Loop through the files and create the vrf data for querying
		for i, f := range files {
			fileName = append(fileName, f.Name())
			// fmt.Println("\n", fileName)
			fileContent, err := ioutil.ReadFile(filePath + "/" + f.Name())
			if err != nil {
				fmt.Println(err, i)
			}
			VrfData := strings.Fields(string(fileContent))
			//Middlman variables for dynamic vrf struct building
			var vlan int
			var vrfname string
			var gateway string
			var cidr int
			//Validate if the data being ingested is Cisco and leverage the cisco formats
			if isCisco(VrfData, "show") {
				for d := range VrfData {
					//Gather the Data for the Interface and strip non numerica characters and convert the string to an integer
					if VrfData[d] == "interface" {
						re := regexp.MustCompile(`[-]?\d[\d,]*[\.]?[\d{2}]*`)
						conv, e := strconv.Atoi(strings.Join(re.FindAllString(VrfData[d+1], -1), ","))
						if e != nil {
							log.Print("Atoi Conversion Failed", e)
						}
						vlan = conv
						//log.Println(conv)
					}
					if VrfData[d] == "member" {
						//log.Println(VrfData[d+1])
						vrfname = VrfData[d+1]
					}
					if strings.Contains(VrfData[d], "/") {
						fullCidr := VrfData[d]
						gatewayCIDR := strings.Split(fullCidr, "/")
						//log.Println(fullCidr)
						gateway = gatewayCIDR[0]
						//log.Println(gatewayCIDR[1])
						cidr1 := gatewayCIDR[1]
						cidr, err = strconv.Atoi(cidr1)
						if err != nil {
							log.Print("Atoi Conversion Failed", err)
						}
						hosts, submask := cidrHosts(fullCidr)
						hostBegin := hosts[0]
						hostEnd := hosts[len(hosts)-1]
						//fmt.Println("\n", len(hosts)-3)
						hostrange := hostBegin + "-" + hostEnd
						//log.Println("/n", "gateway:", gateway, "iprange: ", hostBegin, "-", hostEnd, " subnet:", submask, "/n")
						VrfDataFin := DataStruct{Vlan: vlan, VrfName: vrfname, Gateway: gateway, CIDR: cidr, IpCIDR: fullCidr, SubnetMask: submask, IPRange: hostrange}
						VrfReturn.Data = append(VrfReturn.Data, VrfDataFin)
						//fmt.Println(VrfDataFin)
					}
				}
			} else {
				//Validate if the data is from a checkpoint firewall and execute the data parser
				if isCheckpoint(VrfData) {
					var vrfname string
					var hostrange string
					var submask string
					var fullCidr string
					var hosts []string
					var vlan int
					var cidr int
					var gateway string
					for d := range VrfData {

						if strings.Contains(VrfData[d], "/") {
							fullCidr = VrfData[d]
							cidr1 := strings.Split(VrfData[d], "/")
							cidr, err = strconv.Atoi(cidr1[1])
							if err != nil {
								log.Print("Atoi Conversion Failed", err)
							}

							hosts, submask = cidrHosts(fullCidr)
							hostBegin := hosts[0]
							hostEnd := hosts[len(hosts)-1]
							hostrange = hostBegin + "-" + hostEnd
						}
						if strings.Contains(VrfData[d], "bond") {
							bondo := strings.Split(VrfData[d], ".")
							vlan, err = strconv.Atoi(bondo[1])
							if err != nil {
								log.Print("Atoi Conversion Failed", err)
							}
							vrfname = "Checkpoint-vlan" + bondo[1]
						}
						//Needs better error checking to validate missing data points and return ingestion errors and not pollute the struct
						if strings.Contains(VrfData[d], "gateway") {
							gateway = VrfData[d+1]
							VrfDataFin := DataStruct{Vlan: vlan, VrfName: vrfname, Gateway: gateway, CIDR: cidr, IpCIDR: fullCidr, SubnetMask: submask, IPRange: hostrange}
							VrfReturn.Data = append(VrfReturn.Data, VrfDataFin)
							//fmt.Println(VrfDataFin)
						}

					}
				}
			}
		}
	}
}

//Is Cisco Function for checking for string fields and validating the data came from a cisco
func isCisco(s []string, str string) bool {
	for v := range s {
		if s[v] == str {
			return true
		}
	}
	return false
}

//Checking for specific fields to validate if the data is from checkpoint, bond is searched for because it contains the vlan example: bond1.42 is 1st interface and vlan 42
func isCheckpoint(s []string) bool {
	for v := range s {
		if strings.Contains(s[v], "bond") {
			return true
		}
	}
	return false
}
