package main

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"strings"
)

//Subnet Mask
func ipv4MaskString(m []byte) string {
	if len(m) != 4 {
		panic("ipv4Mask: len must be 4 bytes")
	}
	return fmt.Sprintf("%d.%d.%d.%d", m[0], m[1], m[2], m[3])
}

/// Getting the Cidr and Converting it to IP Addresses
func cidrHosts(netw string) ([]string, string) {
	// convert string to IPNet struct
	_, ipv4Net, err := net.ParseCIDR(netw)
	if err != nil {
		log.Print(err, " :Error in cidrHosts function on net.ParseCIDR the network data")
	}
	// convert IPNet struct mask and address to uint32
	mask := binary.BigEndian.Uint32(ipv4Net.Mask)
	// find the start IP address
	start := binary.BigEndian.Uint32(ipv4Net.IP)
	// find the final IP address
	finish := (start & mask) | (mask ^ 0xffffffff)
	// make a slice to return host addresses
	var hosts []string
	// loop through addresses as uint32.
	// I used "start + 1" and "finish - 1" to discard the network and broadcast addresses.
	for i := start + 1; i <= finish-1; i++ {
		// convert back to net.IPs
		// Create IP address of type net.IP. IPv4 is 4 bytes, IPv6 is 16 bytes.
		ip := make(net.IP, 4)
		binary.BigEndian.PutUint32(ip, i)
		hosts = append(hosts, ip.String())
	}
	subnetMask := ipv4MaskString(ipv4Net.Mask)
	// return a slice of strings containing IP addresses
	return hosts, subnetMask
}

//Working Code Commented out to work around the bug
//Check to see if the address is in the subnet by comparing the 3rd octet (Could be a possible bug in the design when it comes to /23 's and below)
// func IpChecker(GivenAddress string, DataTable DataStruct) bool {
// 	IpSplitter := strings.Split(GivenAddress, ".")
// 	if strings.Contains(DataTable.IpCIDR, IpSplitter[2]) {
// 		v, ipv4Net, err := net.ParseCIDR(DataTable.IpCIDR)
// 		if err != nil {
// 			log.Print(err, v, " : In the IpChecker Function parsing the Cidr")
// 		}
// 		fmt.Printf("%t", ipv4Net)
// 		ipaddr := net.ParseIP(GivenAddress)
// 		if ipv4Net.Contains(ipaddr) {
// 			return true
// 		}
// 	}
// 	return false
// }

// func IpChecker(GivenAddress string, DataTable DataStruct) bool {
//Bug Workaround
func IpChecker(GivenAddress string, DataTable DataStruct) bool {
	v, ipv4Net, err := net.ParseCIDR(DataTable.IpCIDR)
	if err != nil {
		log.Print(err, v, " : In the IpChecker Function parsing the Cidr")

	}
	ipaddr := net.ParseIP(GivenAddress)
	return ipv4Net.Contains(ipaddr)
}

//Return VRF Data based on the input
func IpValJson(vrfdata DataReturned, ip string) (jsonme []byte) {
	errorStruct := map[string]string{"Error": ""}
	//easter egg code can be removed to the point of return jsonDance
	if strings.Contains(ip, "dancing") || strings.Contains(ip, "dance") {
		errorStruct := map[string]string{"Error": "Just Dance - https://github.com/jmhobbs/party-gopher/blob/master/dancing-gopher.gif"}
		jsonDance, err := json.Marshal(errorStruct)
		if err != nil {
			log.Print(err)
		}
		return jsonDance
		//remove } else if planning on removing the easter egg
	} else if net.ParseIP(ip) == nil {
		errorStruct = map[string]string{"Error": ip + " - Not a valid IP for the request"}
		jsonInvalidReq, err := json.Marshal(errorStruct)
		if err != nil {
			log.Print(err)
			return []byte("json marsheling error check logs")
		}
		return jsonInvalidReq
	} else {
		for ds := range vrfdata.Data {
			if IpChecker(ip, vrfdata.Data[ds]) {
				jsonMe, err := json.Marshal(vrfdata.Data[ds])
				if err != nil {
					log.Print(err)
				}
				return jsonMe
			}
		}
		errorStruct = map[string]string{"Error": ip + " - Not Found in Data Pages"}
		jsonNotFound, err := json.Marshal(errorStruct)
		if err != nil {
			log.Print(err)
			return []byte("json marsheling error check logs")
		}
		return jsonNotFound
	}
}
