package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

//Config Variable for Later
var filePath = "./data_ing"

// Internal Testing IP
var ip = "192.33.63.1"

type DataStruct struct {
	Vlan       int
	VrfName    string
	Gateway    string
	CIDR       int
	IpCIDR     string
	SubnetMask string
	IPRange    string
}
type DataReturned struct {
	Data []DataStruct
}

var VrfReturn DataReturned

var fileName []string

//The vrf struct used for building the data needed for the application
type Vrf struct {
	VrfName    string
	Cidr       string
	SubnetMask string
	Gateway    string
}

// HandleIP Pass the Data to the VRF function for return data on where the ip is located
func HandleIp(w http.ResponseWriter, r *http.Request) {
	httpdata := mux.Vars(r)
	ip = httpdata["ip"]
	ipret := IpValJson(VrfReturn, ip)
	w.Write(ipret)
}

// HandleIngest Parse Files without having to restart the application through URL Get
func HandleIngest(w http.ResponseWriter, r *http.Request) {
	httpdata := mux.Vars(r)
	if httpdata["true"] == "true" || httpdata["true"] == "True" {
		FileParse(filePath, &VrfReturn)
		w.Write([]byte("Data Ingesting Completed on folder located in " + filePath + " "))
	} else {
		errorStruct := map[string]string{"Error": httpdata["true"] + " - Not a valid request if you wish to reingest please put True or true"}
		jsonInvalidReq, err := json.Marshal(errorStruct)
		if err != nil {
			log.Print(err)
		}
		w.Write(jsonInvalidReq)
	}
}

func main() {

	// Read the contents of the directory from the variable filePath
	FileParse(filePath, &VrfReturn)
	// log.Print("ip: ", ip)
	// log.Print(string(IpValJson(VrfReturn, ip)))

	// Creating the router and end points
	r := mux.NewRouter()
	srv := &http.Server{
		Handler: r,
		Addr:    "127.0.0.1:8000",
		// Time outs on HTTP applications are good juju for security and performance reasons
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	// Handle the Functions for the router through get methods
	r.HandleFunc("/iprequest/{ip}", HandleIp).Methods("GET", "PUT", "POST")
	r.HandleFunc("/dataing/{true}", HandleIngest).Methods("GET")
	log.Print("Listening on 127.0.0.1:8000/...")
	log.Fatal(srv.ListenAndServe())

}
